import torch
import torch.nn as nn
import backbones


class BaselineModel(nn.Module):
    '''
    num_label is label numbers, which defines output number

    '''
    def __init__(self, num_label=1, base_net='mlnn', finetuning=False, **kwargs):
        super(BaselineModel, self).__init__()
        self.num_label = num_label
        self.base_net=base_net
        self.base_network = backbones.get_backbone(base_net, **kwargs)
        feature_dim = self.base_network.output_num()
        self.kwargs = kwargs
        

        #self.output_layer = nn.Linear(feature_dim, num_label)
        output_list = [
                nn.Linear(feature_dim,100),
                nn.Dropout(p=0.2),
                nn.ReLU(),
                nn.Linear(100,50),
                nn.Dropout(p=0.2),
                nn.ReLU(),
                nn.Linear(50, 20),
                nn.Dropout(p=0.2),
                nn.ReLU(),
                nn.Linear(20, num_label)
            ]
        self.output_layer = nn.Sequential(*output_list)
        self.output_layer_input_dim = [feature_dim, 100]
        self.criterion = torch.nn.MSELoss()
        self.finetuning=finetuning

    def forward(self, samples):
        target_reg, target_label_reg = samples['tre']
        if(self.base_net=="transformer"):
            target_reg = self.base_network(target_reg,target_label_reg)
        else:
            target_reg = self.base_network(target_reg)
        target_reg = self.output_layer(target_reg)
        reg_loss = self.criterion(target_reg, target_label_reg)
        transfer_loss = 0*reg_loss

        return reg_loss, transfer_loss # 0 indicates transfer loss

    def predict(self, x):
        if(self.base_net=="transformer"):
            y = torch.zeros(x.shape[:-1]).unsqueeze(-1).to(self.kwargs['device'])
            features = self.base_network(x,y)
        else:
            features = self.base_network(x)

        reg = self.output_layer(features)
        return reg

    def get_parameters(self, initial_lr=1.0):
        if(self.finetuning):# only train the output layer parameters
            print('Fine-tuning a trained model')
            params = [
                {'params': self.output_layer.parameters(), 'lr': 1.0 * initial_lr}
                ]
        else:
            params = [
                {'params': self.base_network.parameters(), 'lr': initial_lr},
                {'params': self.output_layer.parameters(), 'lr': initial_lr},
            ]

        return params

